if(!global.Promise) {
  global.Promise = require('bluebird');
}
var chai = require('chai')
  , chaiHttp = require('chai-http');
chai.use(chaiHttp);
var expect = require('chai').expect;

var app = require('../app.js');

describe('homepage', function(){
  it('should respond to GET',function(){
    return chai.request(app)
      .get('/')
      .then(function(res){
        return expect(res.status).to.equal(200);
    });
  });
});
